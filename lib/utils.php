<?php
// Add common assets
$f3->get('assets')->add('/assets/css/bootstrap.min.css', 'css', 'head');
$f3->get('assets')->add('/assets/css/propeller.min.css', 'css', 'head');
$f3->get('assets')->add('/assets/css/styles.css?v=201906061620', 'css', 'head');

$f3->get('assets')->add('/assets/js/bootstrap.min.js', 'js', 'footer');
$f3->get('assets')->add('/assets/js/propeller.min.js', 'js', 'footer');
$f3->get('assets')->add('/assets/js/main.js?v=20190520', 'js', 'footer');

// Create DB object
$f3->set('db', new DB\SQL(
    'mysql:host=' . $f3->get('dbconfig.host') .
    ';port=' . $f3->get('dbconfig.port') .
    ';dbname=' . $f3->get('dbconfig.dbname'),
    $f3->get('dbconfig.user'),
    $f3->get('dbconfig.password')
));

// Global
$f3->set('userip',
    isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ?
        $_SERVER["HTTP_CF_CONNECTING_IP"] : !empty($_SERVER['HTTP_CLIENT_IP']) ?
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
);
$f3->set('pageTitle', 'CTOON');
$f3->set('pageDesc', 'Enjoy watching cartoons like it should already be: easy and clean.');
$f3->set('ogImage', 'https://' . $f3->get('SERVER.SERVER_NAME') . '/assets/img/ctoon-titlecard.png');
$f3->set('ogImageAlt', 'CTOON.party');

// Detect if we need MP4 only
$ua = $_SERVER['HTTP_USER_AGENT'];
// Default value
$f3->set('forceMP4', false);
// Exclusion
if (!strstr($ua, "Chrome") && !strstr($ua, "Vivaldi") && !strstr($ua, "Opera")) {
    // If Safari or IE
    if (strstr($ua, "AppleWebKit/") || strstr($ua, "MSIE")) {
        $f3->set('forceMP4', true);
    }
}

// Convert language code to something more pretty
function langParse($code)
{
    switch ($code) {
        case 'en':
            return 'English';
            break;
        case 'fr':
            return 'Français';
            break;
        case 'pt':
            return 'Português';
            break;
        default:
            return $code;
    }
}

// Parse season numbers
function seasonParse($season, $fulltext = false)
{
    switch ($season) {
        case 100:
            return 'Extra';
            break;
        case 101:
            return 'Movie';
            break;
        default:
            return ($fulltext ? 'Season ' . $season : $season);
    }
}

// Parse season and episode numbers to text or array
function parseSE($season, $episode, $options = false)
{
    $episode = str_replace("_", " & ", $episode);
    // This change special codes on "season" for extras or movies
    if ($season == '100') {
        if ($options == 'array') {
            return array('Extras', $episode);
        } else {
            return 'Extra ' . $episode;
        }
    } elseif ($season == '101') {
        if ($options == 'array') {
            return array('Movies', $episode);
        } else {
            return 'Movie ' . $episode;
        }
    } else {
        if ($options == 'short') {
            return $season . 'x' . $episode;
        } elseif ($options == 'array') {
            return array('Season ' . $season, $episode);
        } else {
            return 'Season ' . $season . ' Episode ' . $episode;
        }
    }
}
