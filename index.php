<?php
// Hello Composer
require __DIR__ . '/vendor/autoload.php';

// Hello Whoops
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// Hello Fat Free Framework
$f3 = \Base::instance();

// Here's the config
$f3->config(__DIR__ . '/app/config/config.ini');

// Tor redirect
if ($f3->get('SERVER.HTTP_CF_IPCOUNTRY') == 'T1' && $f3->get('SERVER.HTTP_HOST') != $f3->get('remote.tor_host')) {
  header('Location: http://' . $f3->get('remote.tor_host') . $f3->get('SERVER.REQUEST_URI'));
  exit;
}

// Plugins..
// f3-assets
$f3->set('assets', \Assets::instance());

// Utilities
require __DIR__ . '/lib/simple-rate-limiter/RateLimiter.php';
require __DIR__ . '/lib/utils.php';

// Hello World
$f3->run();
