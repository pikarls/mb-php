<?php
// THIS IS A HUGE WORK IN PROGRESS
// AND JUST A WAY TO DO SOME TESTS
class Api2
{
    public function beforeRoute($f3)
    {
        header("Access-Control-Allow-Origin: *", true);
        header("Content-Type: application/json", true);
    }

    public function _show($show)
    {
        return array(
            "id" => (int) $show['showId'],
            "short_name" => $show['showShortName'],
            "name" => $show['showName'],
            "rating" => $show['showRating'] ? $show['showRating'] : 'TV-Y7',
            "thumbnail" => "https://ctoon.party/assets/img/shows/" . $show['showShortName'] . ".jpg",
            "cover" => "https://ctoon.party/assets/img/shows/" . $show['showShortName'] . ".jpg",
            "description" => $show['showDescription'],
            "description_source" => $show['showDescriptionSource'],
            "links" => array(
                "official" => $show['showLink'],
                "fan_news" => $show['showLinkFanNews'],
                "imdb" => 'http://www.imdb.com/title/' . $show['showImdb'],
                "itunes" => $show['showLinkItunes'],
                "amazon" => $show['showLinkAmazon'],
                "google_play" => $show['showLinkPlay'],
                "netflix" => (bool) $show['showIsOnNetflix'],
            ),
        );
    }

    public function _episode($episode)
    {
        $s = strtoupper($episode['showShortName']);
        $arr = array(
            "id" => (int) $episode['epiNumberTotal'],
            "title" => (string) $episode['epiName'],
            "season" => (string) seasonParse($episode['epiSeason']),
            "episode" => (string) $episode['epiNumber'],
            "sxe" => array(
                "short" => (string) parseSE($episode['epiSeason'], $episode['epiNumber'], 'short'),
                "long" => (string) parseSE($episode['epiSeason'], $episode['epiNumber'])
            ),
            "published_date" => (string) $episode['epiPublishedDate'],
            "files" => array(
                "hls" => (string) 'https://files.ctoon.party/hls/' . $s. '/EN/' . $s . '-' . $episode['epiSeason'] . 'x' . $episode['epiNumber'] . '-EN-WEBDL/playlist.m3u8'
            )
        );

        return $arr;
    }

    public function _orderEpisodes($episodes) {
        $content = [];
        $content['episodes'] = [];
        $content['extras'] = [];
        $content['movies'] = [];
        $content['books'] = [];
        $content['music'] = [];

        foreach ($episodes as $episode) {
            $s = seasonParse($episode['epiSeason'], true);
            if ($s == 'Extra') {
                if (!array_key_exists($s, $content['extras'])) {
                    $content['extras'][$s] = [];
                }

                array_push($content['extras'][$s], $this->_episode($episode));
            } else if ($s == 'Movie') {
                if (!array_key_exists($s, $content['movies'])) {
                    $content['movies'][$s] = [];
                }

                array_push($content['movies'][$s], $this->_episode($episode));
            } else {
                if (!array_key_exists($s, $content['episodes'])) {
                    $content['episodes'][$s] = [];
                }

                array_push($content['episodes'][$s], $this->_episode($episode));
            }
        }

        return $content;
    }

    public function getList($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('api2List-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api2/list. Try again in 5 minutes.', 429);
            return;
        }

        $json['latest'] = $json['shows'] = [];

        $shows = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showIsHidden = 0 ORDER BY showPriority;'
        );
        foreach ($shows as $show) {
            $i = $show['showShortName'];
            $json['shows'][$i]['show'] = $this->_show($show);

            $episodes = $f3->get('db')->exec(
                'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE epiShow = ? ORDER BY epiSeason, epiNumber;',
                $show['showId']
            );

            $json['shows'][$i]['content'] = $this->_orderEpisodes($episodes);
        }

        $episodes = $f3->get('db')->exec(
            'SELECT * FROM episodes ORDER BY epiPublishedDate DESC LIMIT 10;'
        );
        foreach ($episodes as $episode) {
            $ep['episode'] = $this->_episode($episode);

            $show = $f3->get('db')->exec(
                'SELECT * FROM shows WHERE showId = ?;',
                $episode['epiShow']
            )[0];

            $ep['show'] = $this->_show($show);

            array_push($json['latest'], $ep);
        }

        echo json_encode($json);
    }

    public function getFullShow($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('api2FullShow-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api2/show/:show. Try again in 5 minutes.', 429);
            return;
        }

        $json = $episodeList = [];

        $show = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showShortName = ?;',
            $f3->get('PARAMS.show')
        )[0];
        $json['show'] = $this->_show($show);

        $episodes = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE epiShow = ? ORDER BY epiSeason, epiNumber;',
            $show['showId']
        );

        $json['content'] = $this->_orderEpisodes($episodes);

        echo json_encode($json);
    }

    public function getContent($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('api2Content-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/details/:id. Try again in 5 minutes.', 429);
            return;
        }

        $json = [];

        echo json_encode($json);
    }

    public function getEpisode($f3, $args)
    {
        $limit = new RateLimiter(300, 50);
        if (!$limit->check('api2Episode-' . $f3->get('userip'))) {
            jsonError('API Limit reached for /api/:show/:episode. Try again in 5 minutes.', 429);
            return;
        }

        $json = [];
        $json['_prev'] = $json['_next'] = null;

        $show = $f3->get('db')->exec(
            'SELECT * FROM shows WHERE showShortName = ?;',
            $f3->get('PARAMS.show')
        )[0];
        $json['show'] = $this->_show($show);

        $episode = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal = ? LIMIT 1;',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        $json['episode'] = $this->_episode($episode);

        $prev = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal < ? ORDER BY epiNumberTotal DESC LIMIT 1',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        if ($prev) $json['_prev'] = $this->_episode($prev);

        $next = $f3->get('db')->exec(
            'SELECT * FROM episodes INNER JOIN shows ON showId = epiShow WHERE showShortName = ? AND epiNumberTotal > ? ORDER BY epiNumberTotal ASC LIMIT 1',
            array($f3->get('PARAMS.show'), $f3->get('PARAMS.episode'))
        )[0];
        if ($next) $json['_next'] = $this->_episode($next);

        echo json_encode($json);
    }
}
// THIS IS A HUGE WORK IN PROGRESS
// AND JUST A WAY TO DO SOME TESTS
