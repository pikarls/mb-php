<?php
class Post
{
    public function setVote($f3, $args)
    {
        echo "ok";
    }

    public function sendReport($f3, $args)
    {
        $j = json_decode(file_get_contents('php://input'));
        $r = new DB\SQL\Mapper($f3->get('db'), 'reports');

        $r->reset();
        $r->ip = $f3->get('userip');
        $r->problem = $j->problem;
        $r->description = $j->description;
        $r->links = $j->links;
        $r->forceMP4 = $j->forceMP4;
        $r->ua = $j->ua;

        $r->save();

        $data = array(
            "embeds" => array(
                array(
                    "title" => "New report from " . $r->ip,
                    "fields" => array(
                        array(
                            "name" => "problem",
                            "value" => $r->problem,
                            "inline" => true,
                        ),
                        array(
                            "name" => "forceMP4",
                            "value" => $r->forceMP4 ? 'True' : 'False',
                            "inline" => true,
                        ),
                        array(
                            "name" => "Description",
                            "value" => $r->description ? $r->description : '_None_',
                        ),
                        array(
                            "name" => "Links",
                            "value" => $r->links ? $r->links : '_None_',
                        ),
                        array(
                            "name" => "UA",
                            "value" => $r->ua,
                        ),
                    ),
                ),
            ),
        );

        $curl = curl_init($f3->get('discord.webhook'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($curl);
        echo 'OK';
    }

    public function sendLove($f3, $args)
    {
        $r = new DB\SQL\Mapper($f3->get('db'), 'love');
        $r->load(
            array('loveIP=?', $f3->get('userip'))
        );

        // If it doesn't exists, create it
        if ($r->dry()) {
            $r->reset();
            $r->loveCount = 0;
        } else {
            // And check if it was sent recently
            $loveD = new DateTime($r->loveTS);
            if ($loveD >= (new DateTime())->setTime(0, 0)) {
                exit('You already sent love today.');
            }
        }

        // Set values
        $r->loveIP = $f3->get('userip');
        $r->loveLoc = $f3->get('SERVER.HTTP_CF_IPCOUNTRY');
        $r->loveCount++;
        $r->save();

        $data = array(
            "embeds" => array(
                array(
                    "title" => "Someone sent love!",
                    "description" => "Right from " . $f3->get('SERVER.HTTP_CF_IPCOUNTRY'),
                    "timestamp" => date('c'),
                    "footer" => array(
                        "text" => "Count: " . $r->loveCount,
                    ),
                ),
            ),
        );

        $curl = curl_init($f3->get('discord.webhook'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($curl);
        echo 'Love sent! Thank you!';
    }
}
