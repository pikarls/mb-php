<?php
// This is for developing using PHP's webserver
if (file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI'])) {
    return false; // serve the requested resource as-is.
}

if (php_sapi_name() == 'cli-server') {
    include_once 'index.php';
}
